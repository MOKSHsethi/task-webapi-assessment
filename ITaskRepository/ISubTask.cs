﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ISubTask
    {
        SubTask Create (SubTask subTask);

        List<SubTask> GetSubTask();

        SubTask GetSubTaskById(int id);
    }
}
