﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TaskApi.Models;

namespace TaskApi.Context
{
    public class TaskDbContext:DbContext
    {
        public TaskDbContext()
        {

        }
        public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options) { }

        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<SubTask> SubTask { get; set; }
    }
}
