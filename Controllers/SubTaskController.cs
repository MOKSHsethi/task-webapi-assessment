﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubTaskController : ControllerBase
    {
        ISubTask _repo;
        public SubTaskController(ISubTask repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<List<SubTask>> GetTask()
        {
            if (_repo.GetSubTask().ToList().Count == 0)
                return NotFound();
            else

                return _repo.GetSubTask();
        }

        [HttpGet("{id}")]

        public ActionResult<int> GetSubTaskById(int id)
        {
            if (_repo.GetSubTaskById(id) == null)
                return 0;
            else
                return Ok(_repo.GetSubTaskById(id));
        }

        [HttpPost]
        public ActionResult<int> Create(SubTask Subtasks)
        {
            _repo.Create(Subtasks);
            return Created("Created", Subtasks);
        }
    }
}
