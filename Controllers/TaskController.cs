﻿using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        ITask _repo;
        public TaskController(ITask repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public ActionResult<List<Tasks>> GetTask()
        {
            if (_repo.GetTask().ToList().Count == 0)
                return NotFound();
            else

                return _repo.GetTask();
        }

        [HttpGet("{id}")]

        public ActionResult<int> GetTaskById(int id)
        {
            if (_repo.GetTaskById(id) == null)
                return 0;
            else
                return Ok(_repo.GetTaskById(id));
        }

        [HttpPost]
        public ActionResult<int> Create(Tasks tasks)
        {
            _repo.Create(tasks);
            return Created("Created", tasks);
        }

        [HttpPut("{id}")]
        public void EditTAsk(int id, Tasks tasks)
        {
            _repo.EditTAsk(id, tasks);

        }
        [HttpDelete("{id}")]

        public ActionResult<string> Delete(int id)
        {
            Tasks task = _repo.GetTaskById(id);
            if (task == null)
                return "There is no record";
            else
            {

                _repo.Delete(id);
                return Ok("Record deleted");
            }
        }







    }
}
