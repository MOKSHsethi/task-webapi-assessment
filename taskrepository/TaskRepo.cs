﻿using System.Threading.Tasks;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class TaskRepo : ITask
    {

        TaskDbContext _db;
        public TaskRepo(TaskDbContext db)
        {
            _db = db;
        }

        public Tasks Create(Tasks task)
        {
            _db.Tasks.Add(task);
            _db.SaveChanges();
            return task;
            
        }

        public int Delete(int id)
        {
            Tasks obj = GetTaskById(id);
            if (obj != null)
            {
                _db.Tasks.Remove(obj);
                _db.SaveChanges();
                return 0;
            }
            else

                return 1;
        }

        public int EditTAsk(int id, Tasks task)
        {
           Tasks obj = GetTaskById(id);
            if (obj != null)
            {
                foreach (Tasks temp in _db.Tasks)
                {
                    if (temp.TaskId == id)
                    {
                       
                    }
                }
                _db.SaveChanges();
                return 0;
            }
            else

                return 1;
        }

        public List<Tasks> GetTask()
        {
            return _db.Tasks.ToList();
        }

        public Tasks GetTaskById(int id)
        {
            return _db.Tasks.FirstOrDefault(x => x.TaskId == id);
        }
    }
}
