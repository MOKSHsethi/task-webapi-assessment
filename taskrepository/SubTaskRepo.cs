﻿using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class SubTaskRepo : ISubTask
    {
        TaskDbContext _db;
        public SubTaskRepo(TaskDbContext db)
        {
            _db = db;
        }
        public SubTask Create(SubTask subTask)
        {
            _db.Add(subTask);
            _db.SaveChanges();
            return subTask;
        }

        public List<SubTask> GetSubTask()
        {
            return _db.SubTask.ToList();    
        }

        public SubTask GetSubTaskById(int id)
        {
            return _db.SubTask.FirstOrDefault(x => x.SubTaskId == id);
        }
    }
}
