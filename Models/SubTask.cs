﻿namespace TaskApi.Models
{
    public class SubTask
    {// Add properties
        // Id, SubTaskName, Created By, Created On, Description,
        // TaskId


        public int SubTaskId { get; set; }

        public string SubTaskName { get; set; }

        public string Description { get; set; }

        public DateTime Created_by { get; set; }

        public DateTime Created_On { get; set; }

        public Tasks TaskID { get; set; }


    }
}
