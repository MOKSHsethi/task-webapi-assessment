﻿using System.ComponentModel.DataAnnotations;

namespace TaskApi.Models
{
    public class Tasks
    {
        // Add properties
        // Id, Name, Created By, Created On, Description

        [Key]
        public int TaskId { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Created_On { get; set;}
        public string Created_By { get; set; }

    }
}
